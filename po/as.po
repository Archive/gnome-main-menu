# translation of gnome-main-menu.po.master.as.po to Assamese
# Assamese translation of gnome-main-menu.
# Copyright (C) 2009 gnome-main-menu's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-main-menu package.
#
# Amitakhya Phukan <aphukan@fedoraproject.org>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: gnome-main-menu.po.master.as\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-main-menu&component=general\n"
"POT-Creation-Date: 2009-06-08 07:02+0000\n"
"PO-Revision-Date: 2009-06-12 15:14+0530\n"
"Last-Translator: Amitakhya Phukan <aphukan@fedoraproject.org>\n"
"Language-Team: Assamese\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../application-browser/etc/application-browser.desktop.in.in.h:1
#: ../application-browser/src/application-browser.c:86
msgid "Application Browser"
msgstr "অনুপ্ৰয়োগ চৰক"

#: ../application-browser/etc/application-browser.schemas.in.h:1
msgid "Exit shell on add or remove action performed"
msgstr "সংযোজন অথবা অপসাৰণৰ কাৰ্য্য কৰিলে শ্বেলৰ পৰা প্ৰস্থান কৰা হ'ব"

#: ../application-browser/etc/application-browser.schemas.in.h:2
msgid "Exit shell on help action performed"
msgstr "সহায়ৰ কাৰ্য্য কৰিলে শ্বেলৰ পৰা প্ৰস্থান কৰা হ'ব"

#: ../application-browser/etc/application-browser.schemas.in.h:3
msgid "Exit shell on start action performed"
msgstr "আৰম্ভণিত কাৰ্য্য কৰিলে শ্বেলৰ পৰা প্ৰস্থান কৰা হ'ব"

#: ../application-browser/etc/application-browser.schemas.in.h:4
msgid "Exit shell on upgrade or uninstall action performed"
msgstr "উন্নয়ন অথবা সংস্থাপন আঁতৰুৱা কাৰ্য্য কৰিলে শ্বেলৰ পৰা প্ৰস্থান কৰা হ'ব"

#: ../application-browser/etc/application-browser.schemas.in.h:5
msgid "Filename of existing .desktop files"
msgstr "উপস্থিত .desktop নথিপত্ৰৰ নাম"

#: ../application-browser/etc/application-browser.schemas.in.h:6
msgid "Indicates whether to close the shell when a help action is performed"
msgstr "সহায়ৰ কাৰ্য্য কৰিলে শ্বেল বন্ধ কৰা হ'ব নে নহয় সেইটো চিহ্নিত কৰিবলৈ ব্যৱহৃত হয়"

#: ../application-browser/etc/application-browser.schemas.in.h:7
msgid "Indicates whether to close the shell when a start action is performed"
msgstr "আৰম্ভণিৰ কাৰ্য্য কৰিলে শ্বেল বন্ধ কৰা হ'ব নে নহয় সেইটো চিহ্নিত কৰিবলৈ ব্যৱহৃত হয়"

#: ../application-browser/etc/application-browser.schemas.in.h:8
msgid ""
"Indicates whether to close the shell when an add or remove action is "
"performed"
msgstr ""
"সংযোজন অথবা অপসাৰণৰ কাৰ্য্য কৰিলে শ্বেল বন্ধ কৰা হ'ব নে নহয় সেইটো চিহ্নিত কৰিবলৈ "
"ব্যৱহৃত হয়"

#: ../application-browser/etc/application-browser.schemas.in.h:9
msgid ""
"Indicates whether to close the shell when an upgrade or uninstall action is "
"performed"
msgstr ""
"উন্নয়ন অথবা সংস্থাপন আঁতৰুৱা কাৰ্য্য কৰিলে শ্বেল বন্ধ কৰা হ'ব নে নহয় সেইটো চিহ্নিত কৰিবলৈ "
"ব্যৱহৃত হয়"

#: ../application-browser/etc/application-browser.schemas.in.h:10
msgid "Max number of New Applications"
msgstr "নতুন অনুপ্ৰয়োগৰ সৰ্বাধিক সংখ্যা"

#: ../application-browser/etc/application-browser.schemas.in.h:11
msgid ""
"The maximum number of applications that will be displayed in the New "
"Applications category"
msgstr "নতুন অনুপ্ৰয়োগ বিভাগত প্ৰদৰ্শনযোগ্য সৰ্বাধিক অনুপ্ৰয়োগৰ সংখ্যা"

#: ../application-browser/src/application-browser.c:78
msgid "New Applications"
msgstr "নতুন অনুপ্ৰয়োগ"

#: ../application-browser/src/application-browser.c:83
msgid "Filter"
msgstr "ফিল্টাৰ"

#: ../application-browser/src/application-browser.c:83
msgid "Groups"
msgstr "সংকলন"

#: ../application-browser/src/application-browser.c:83
msgid "Application Actions"
msgstr "অনুপ্ৰয়োগ সংক্ৰান্ত কাৰ্য্য"

#. make start action
#: ../libslab/application-tile.c:372
#, c-format
msgid "<b>Start %s</b>"
msgstr "<b>%s আৰম্ভ কৰক</b>"

#: ../libslab/application-tile.c:391
#: ../main-menu/src/main-menu-migration.c:131
#: ../main-menu/src/main-menu-migration.c:255
#: ../main-menu/etc/system-items.xbel.in.h:1
msgid "Help"
msgstr "সহায়"

#: ../libslab/application-tile.c:438
msgid "Upgrade"
msgstr "উন্নয়ন কৰক"

#: ../libslab/application-tile.c:453
msgid "Uninstall"
msgstr "সংস্থাপন আঁতৰাওক"

#: ../libslab/application-tile.c:780 ../libslab/document-tile.c:715
#: ../nautilus-main-menu/nautilus-main-menu.c:124
#: ../nautilus-main-menu/nautilus-main-menu.c:129
msgid "Remove from Favorites"
msgstr "পছন্দৰ তালিকাৰ পৰা আঁতৰাওক"

#: ../libslab/application-tile.c:782 ../libslab/document-tile.c:717
#: ../nautilus-main-menu/nautilus-main-menu.c:114
#: ../nautilus-main-menu/nautilus-main-menu.c:119
msgid "Add to Favorites"
msgstr "পছন্দৰ তালিকাত যোগ কৰক"

#: ../libslab/application-tile.c:867
msgid "Remove from Startup Programs"
msgstr "প্ৰাৰম্ভিক প্ৰোগ্ৰামৰ তালিকাৰ পৰা আঁতৰাওক"

#: ../libslab/application-tile.c:869
msgid "Add to Startup Programs"
msgstr "প্ৰাৰম্ভিক প্ৰোগ্ৰামৰ তালিকাত যোগ কৰক"

#: ../libslab/app-shell.c:753
#, c-format
msgid ""
"<span size=\"large\"><b>No matches found.</b> </span><span>\n"
"\n"
" Your filter \"<b>%s</b>\" does not match any items.</span>"
msgstr ""
"<span size=\"large\"><b>কোনো মিল নাই ।</b> </span><span>\n"
"\n"
" \"<b>%s</b>\" ফিল্টাৰ অনুযায়ী কোনো সামগ্ৰী পোৱা নাযায় ।</span>"

#: ../libslab/app-shell.c:903
msgid "Other"
msgstr "অন্যান্য"

#: ../libslab/bookmark-agent.c:1077
msgid "New Spreadsheet"
msgstr "নতুন স্প্ৰেড-ছিট"

#: ../libslab/bookmark-agent.c:1082
msgid "New Document"
msgstr "নতুন নথিপত্ৰ"

#: ../libslab/bookmark-agent.c:1135
msgid "Home"
msgstr "ব্যক্তিগত পঞ্জিকা"

#: ../libslab/bookmark-agent.c:1140 ../main-menu/src/slab-window.glade.h:3
msgid "Documents"
msgstr "নথিপত্ৰ"

#: ../libslab/bookmark-agent.c:1146
msgid "Desktop"
msgstr "ডেস্কটপ"

#: ../libslab/bookmark-agent.c:1153
msgid "File System"
msgstr "নথিপত্ৰপ্ৰণালী"

#: ../libslab/bookmark-agent.c:1157
msgid "Network Servers"
msgstr "নে'টৱৰ্ক সেৱক"

#: ../libslab/bookmark-agent.c:1186
msgid "Search"
msgstr "অনুসন্ধান"

#. make open with default action
#: ../libslab/directory-tile.c:171
#, c-format
msgid "<b>Open</b>"
msgstr "<b>খোলক</b>"

#. make rename action
#: ../libslab/directory-tile.c:190 ../libslab/document-tile.c:231
msgid "Rename..."
msgstr "নাম পৰিবৰ্তন..."

#: ../libslab/directory-tile.c:204 ../libslab/directory-tile.c:213
#: ../libslab/document-tile.c:245 ../libslab/document-tile.c:254
msgid "Send To..."
msgstr "চিহ্নিত স্থানলৈ প্ৰেৰণ..."

#. make move to trash action
#: ../libslab/directory-tile.c:228 ../libslab/document-tile.c:280
msgid "Move to Trash"
msgstr "আবৰ্জনাৰ বাকচলৈ স্থানান্তৰ"

#: ../libslab/directory-tile.c:238 ../libslab/directory-tile.c:457
#: ../libslab/document-tile.c:290 ../libslab/document-tile.c:831
msgid "Delete"
msgstr "আঁতৰাওক"

#: ../libslab/directory-tile.c:533 ../libslab/document-tile.c:979
#, c-format
msgid "Are you sure you want to permanently delete \"%s\"?"
msgstr "আপুনি \"%s\" ক স্থায়ীভাবে আঁতৰাবলৈ নিশ্চিত নে ?"

#: ../libslab/directory-tile.c:534 ../libslab/document-tile.c:980
msgid "If you delete an item, it is permanently lost."
msgstr "কোনো বস্তু আঁতৰুৱা হ'লে সেইটো স্থায়ীৰূপে আঁতৰুৱা যাব ।"

#: ../libslab/document-tile.c:192
#, c-format
msgid "<b>Open with \"%s\"</b>"
msgstr "<b>\"%s\"ৰ সহায়ত খোলক</b>"

#: ../libslab/document-tile.c:204
msgid "Open with Default Application"
msgstr "অবিকল্পিত অনুপ্ৰয়োগৰ সহায়ত খোলক"

#: ../libslab/document-tile.c:215
msgid "Open in File Manager"
msgstr "নথিপত্ৰ পৰিচালন ব্যৱস্থাৰ সহায়ত খোলক"

#: ../libslab/document-tile.c:611
msgid "?"
msgstr "?"

#: ../libslab/document-tile.c:618
msgid "%l:%M %p"
msgstr "%l:%M %p"

#: ../libslab/document-tile.c:626
msgid "Today %l:%M %p"
msgstr "আজি %l:%M %p"

#: ../libslab/document-tile.c:636
msgid "Yesterday %l:%M %p"
msgstr "যোৱাকালি %l:%M %p"

#: ../libslab/document-tile.c:648
msgid "%a %l:%M %p"
msgstr "%a %l:%M %p"

#: ../libslab/document-tile.c:656
msgid "%b %d %l:%M %p"
msgstr "%b %d %l:%M %p"

#: ../libslab/document-tile.c:658
msgid "%b %d %Y"
msgstr "%b %d %Y"

#: ../libslab/search-bar.c:255
msgid "Find Now"
msgstr "এতিয়া অনুসন্ধান কৰক"

#: ../libslab/system-tile.c:128
#, c-format
msgid "<b>Open %s</b>"
msgstr "<b>%s খোলক</b>"

#: ../libslab/system-tile.c:141
#, c-format
msgid "Remove from System Items"
msgstr "প্ৰণালীৰ বিভিন্ন সৰঞ্জামৰ পৰা আঁতৰাওক"

#: ../main-menu/etc/GNOME_MainMenu.server.in.in.h:1
msgid "Default menu and application browser"
msgstr "অবিকল্পিত তালিকা আৰু অনুপ্ৰয়োগ চৰক"

#: ../main-menu/etc/GNOME_MainMenu.server.in.in.h:2
#: ../main-menu/src/main-menu-ui.c:2391 ../main-menu/src/slab-window.glade.h:7
msgid "GNOME Main Menu"
msgstr "GNOME প্ৰধান তালিকা"

#: ../main-menu/etc/GNOME_MainMenu.server.in.in.h:3
msgid "GNOME Main Menu Factory"
msgstr "GNOME Main Menu কলঘৰ"

#: ../main-menu/etc/GNOME_MainMenu.server.in.in.h:4
msgid "Main Menu"
msgstr "প্ৰধান তালিকা"

#: ../main-menu/etc/GNOME_MainMenu_ContextMenu.xml.h:1
msgid "_About"
msgstr "বিষয়ে (_A)"

#: ../main-menu/etc/GNOME_MainMenu_ContextMenu.xml.h:2
msgid "_Open Menu"
msgstr "তালিকা খোলক (_O)"

#: ../main-menu/etc/slab.schemas.in.in.h:1
msgid ".desktop file for the Network Manager editor utility"
msgstr "Network Manager সম্পাদনা ব্যৱস্থাৰ বাবে .desktop নথিপত্ৰ"

#: ../main-menu/etc/slab.schemas.in.in.h:2
msgid ".desktop file for the YaST2 network_devices utility"
msgstr "YaST2 network_devices ব্যৱস্থাৰ বাবে .desktop নথিপত্ৰ"

#: ../main-menu/etc/slab.schemas.in.in.h:3
msgid ".desktop file for the file browser"
msgstr "নথিপত্ৰ চৰকৰ বাবে .desktop নথিপত্ৰ"

#: ../main-menu/etc/slab.schemas.in.in.h:4
msgid ".desktop file for the gnome-system-monitor"
msgstr "gnome-system-monitor-ৰ বাবে .desktop নথিপত্ৰ"

#: ../main-menu/etc/slab.schemas.in.in.h:5
msgid ".desktop file for the net config tool"
msgstr "net বিন্যাস ব্যৱস্থাৰ বাবে .desktop নথিপত্ৰ"

#: ../main-menu/etc/slab.schemas.in.in.h:6
msgid ".desktop path for the application browser"
msgstr "অনুপ্ৰয়োগ চৰকৰ বাবে .desktop নথিপত্ৰ"

#: ../main-menu/etc/slab.schemas.in.in.h:7
msgid ""
"This is the command to execute when the \"Open in File Manager\" menu item "
"is activated."
msgstr ""
"তালিকাৰ পৰা \"নথিপত্ৰ পৰিচালন ব্যৱস্থাৰ সহায়ত খোলক\" ব্যৱহাৰ কৰা হ'লে এই আদেশ "
"সঞ্চালিত হ'ব ।"

#: ../main-menu/etc/slab.schemas.in.in.h:8
msgid ""
"This is the command to execute when the \"Open in File Manager\" menu item "
"is activated. FILE_URI is replaced with a uri corresponding to the dirname "
"of the activated file."
msgstr ""
"তালিকাৰ পৰা \"নথিপত্ৰ পৰিচালন ব্যৱস্থাৰ সহায়ত খোলক\" ব্যৱহাৰ কৰা হ'লে এই আদেশ "
"সঞ্চালিত হ'ব । FILE_URI-ৰ পৰিবৰ্তনে সক্ৰিয় কৰা নথিপত্ৰৰ dirname-ৰ uri ব্যৱহাৰ কৰা "
"হ'ব ।"

#: ../main-menu/etc/slab.schemas.in.in.h:9
msgid ""
"This is the command to execute when the \"Send To...\" menu item is "
"activated."
msgstr "তালিকাৰ পৰা \"চিহ্নিত স্থানলৈ প্ৰেৰণ...\"  ব্যৱহাৰ কৰা হ'লে এই আদেশ সঞ্চালিত হ'ব ।"

#: ../main-menu/etc/slab.schemas.in.in.h:10
msgid ""
"This is the command to execute when the \"Send To...\" menu item is "
"activated. DIRNAME and BASENAME are replaced with the corresponding "
"components of the activated tile."
msgstr ""
"তালিকাৰ পৰা \"চিহ্নিত স্থানলৈ প্ৰেৰণ...\"  ব্যৱহাৰ কৰা হ'লে এই আদেশ সঞ্চালিত হ'ব । "
"FILE_URI-ৰ পৰিবৰ্তনে সক্ৰিয় কৰা নথিপত্ৰৰ dirname-ৰ uri ব্যৱহাৰ কৰা হ'ব ।"

#: ../main-menu/etc/slab.schemas.in.in.h:11
msgid "This is the command to execute when the search entry is used."
msgstr "অনুসন্ধানৰ নিবেশ প্ৰয়োগ কৰা হ'লে এই আদেশ সঞ্চালিত হ'ব ।"

#: ../main-menu/etc/slab.schemas.in.in.h:12
msgid ""
"This is the command to execute when the search entry is used. SEARCH_STRING "
"is replaced with the entered search text."
msgstr ""
"অনুসন্ধানৰ নিবেশ প্ৰয়োগ কৰা হ'লে এই আদেশ সঞ্চালিত হ'ব । SEARCH_STRING-ৰ "
"পৰিবৰ্তে অনুসন্ধানৰ বাবে উল্লিখিত তথ্য ব্যৱহাৰ কৰা হ'ব ।"

#: ../main-menu/etc/slab.schemas.in.in.h:13
msgid "command to uninstall packages"
msgstr "সৰঞ্জাম সংস্থাপন আঁতৰুৱা কৰিবলৈ ব্যৱহৃত আদেশ"

#: ../main-menu/etc/slab.schemas.in.in.h:14
msgid ""
"command to uninstall packages, PACKAGE_NAME is replaced by the name of the "
"package in the command"
msgstr ""
"সৰঞ্জাম সংস্থাপন আঁতৰুৱা কৰিবলৈ ব্যৱহৃত আদেশ, PACKAGE_NAME-ৰ পৰিবৰ্তে আদেশত উল্লিখিত "
"সৰঞ্জামৰ নাম ব্যৱহাৰ কৰা হ'ব"

#: ../main-menu/etc/slab.schemas.in.in.h:15
msgid "command to upgrade packages"
msgstr "সৰঞ্জাম উন্নয়ন কৰাৰ আদেশ"

#: ../main-menu/etc/slab.schemas.in.in.h:16
msgid ""
"command to upgrade packages, PACKAGE_NAME is replaced by the name of the "
"package in the command"
msgstr ""
"সৰঞ্জাম উন্নয়ন কৰাৰ আদেশ, PACKAGE_NAME-ৰ পৰিবৰ্তে আদেশে উল্লিখিত সৰঞ্জামৰ নাম "
"ব্যৱহাৰ কৰা হ'ব"

#: ../main-menu/etc/slab.schemas.in.in.h:17
msgid ""
"contains the list (in no particular order) of allowable file tables to show "
"in the file area. possible values: 0 - show the user-specified or \"Favorite"
"\" applications table, 1 - show the recently used applications table, 2 - "
"show the user-specified or \"Favorite\" documents table, 3 - show the "
"recently used documents table, 4 - show the user-specified of \"Favorite\" "
"directories or \"Places\" table, and 5 - show the recently used directories "
"or \"Places\" table."
msgstr ""
"file area-ত প্ৰদৰ্শনযোগ্য অনুমোদিত নথিপত্ৰ টেবুলৰ তালিকা (কোনো নিৰ্দিষ্ট অনুক্ৰম "
"ব্যৱহাৰ কৰা নহয়) । সম্ভাব্য মান: 0 - ব্যৱহাৰকৰ্তা দ্বাৰা নিৰ্ধাৰিত অথবা \"পছন্দৰ\" "
"অনুপ্ৰয়োগ টেবুল প্ৰদৰ্শন কৰা হ'ব, 1 - সম্প্ৰতি ব্যৱহৃত অনুপ্ৰয়োগৰ টেবুল প্ৰদৰ্শন "
"কৰা হ'ব, 2 - ব্যৱহাৰকৰ্তা দ্বাৰা নিৰ্ধাৰিত অথবা \"পছন্দৰ\" নথিপত্ৰৰ টেবুল প্ৰদৰ্শন "
"কৰা হ'ব, 3 - সম্প্ৰতি ব্যৱহৃত নথিপত্ৰৰ টেবুল প্ৰদৰ্শন কৰা হ'ব, 4 - ব্যৱহাৰকৰ্তা "
"দ্বাৰা নিৰ্ধাৰিত অথবা \"পছন্দৰ\" পঞ্জিকা অথবা \"স্থান\" টেবুল প্ৰদৰ্শন কৰা হ'ব, "
"আৰু 5 - সম্প্ৰতি ব্যৱহৃত পঞ্জিকা অথবা \"স্থান\" টেবুল প্ৰদৰ্শন কৰা হ'ব"

#: ../main-menu/etc/slab.schemas.in.in.h:18
msgid ""
"contains the list of files (including .desktop files) to be excluded from "
"the \"Recently Used Applications\" and \"Recent Files\" lists"
msgstr "\"সম্প্ৰতি ব্যৱহৃত অনুপ্ৰয়োগ\" আৰু \"সৰ্বশেষ ব্যৱহৃত নথিপত্ৰ\"-ৰ তালিকাত অন্তৰ্ভুক্ত নকৰাৰ বাবে চিহ্নিত নথিপত্ৰৰ তালিকা (.desktop নথিপত্ৰ এই তালিকাত অন্তৰ্ভুক্ত) ধাৰণকাৰী"

#: ../main-menu/etc/slab.schemas.in.in.h:19
msgid "determines the limit of items in the file-area."
msgstr "file-area-ত উপস্থিত সামগ্ৰীৰ তালিকাৰ সংখ্যা নিৰ্ধাৰণ কৰে ।"

#: ../main-menu/etc/slab.schemas.in.in.h:20
msgid ""
"determines the limit of items in the file-area. The number favorite items is "
"not limited. This limit applies to the number of recent items, i.e. the "
"number of recent items displayed is limited to max_total_items - the number "
"of favorite items. If the number of favorite items exceeds max_total_items - "
"min_recent_items than this limit is ignored."
msgstr ""
"file-area-ত উপস্থিত সামগ্ৰীৰ তালিকাৰ সংখ্যা নিৰ্ধাৰণ কৰে । পছন্দৰ সামগ্ৰীৰ "
"কোনো সীমা নেই । এই সীমা অকল সম্প্ৰতি ব্যৱহৃত সামগ্ৰীৰ বাবে অৰ্থাৎ max_total_items "
"- পছন্দৰ সামগ্ৰী সংখ্যাৰ, সমান সংখ্যক সম্প্ৰতি ব্যৱহৃত সামগ্ৰী প্ৰদৰ্শন কৰা হয় । "
"max_total_items - min_recent_items সংখ্যাৰৰ পৰা অধিক সংখ্যক পছন্দৰ সামগ্ৰী "
"উপস্থিত থাকিলে এই সীমা উপেক্ষা কৰা হয় ।"

#: ../main-menu/etc/slab.schemas.in.in.h:21
msgid ""
"determines the minimum number of items in the \"recent\" section of the file-"
"area."
msgstr ""
"file-area-ৰ \"সম্প্ৰতি\" বিভাগে উপস্থিত সামগ্ৰীৰ সৰ্বনিম্ন সংখ্যা নিৰ্ধাৰণ কৰিবলৈ "
"ব্যৱহৃত ।"

#: ../main-menu/etc/slab.schemas.in.in.h:22
msgid "determines which types of files to display in the file area"
msgstr "file-area-ত প্ৰদৰ্শনযোগ্য নথিপত্ৰৰ ধৰন নিৰ্ধাৰণ কৰিবলৈ ব্যৱহৃত"

#: ../main-menu/etc/slab.schemas.in.in.h:23
msgid "if true, main menu is more anxious to close"
msgstr "মান true হ'লে, প্ৰধান তালিকা বন্ধ কৰাৰ বাবে প্ৰস্তুত"

#: ../main-menu/etc/slab.schemas.in.in.h:24
msgid ""
"if true, main menu will close under these additional conditions: tile is "
"activated, search activated"
msgstr ""
"মান true হ'লে, অতিৰিক্ত পৰিস্থিতিতে প্ৰধান তালিকা বন্ধ কৰা হ'ব: টাইল সক্ৰিয়, "
"অনুসন্ধান সক্ৰিয় হ'লে"

#: ../main-menu/etc/slab.schemas.in.in.h:25
msgid "lock-down configuration of the file area"
msgstr "file area-ৰ বাবে lock-down বিন্যাস"

#: ../main-menu/etc/slab.schemas.in.in.h:26
msgid "lock-down status for the application browser link"
msgstr "অনুপ্ৰয়োগ চৰক লিংকৰ বাবে lock-down অৱস্থা"

#: ../main-menu/etc/slab.schemas.in.in.h:27
msgid "lock-down status for the search area"
msgstr "search area-ৰ বাবে lock-down অৱস্থা"

#: ../main-menu/etc/slab.schemas.in.in.h:28
msgid "lock-down status for the status area"
msgstr "status area-ৰ বাবে lock-down অৱস্থা"

#: ../main-menu/etc/slab.schemas.in.in.h:29
msgid "lock-down status for the system area"
msgstr "system area-ৰ বাবে lock-down অৱস্থা"

#: ../main-menu/etc/slab.schemas.in.in.h:30
msgid "lock-down status for the user-specified apps section"
msgstr "ব্যৱহাৰকৰ্তা দ্বাৰা চিহ্নিত apps বিভাগৰ বাবে lock-down অৱস্থা"

#: ../main-menu/etc/slab.schemas.in.in.h:31
msgid "lock-down status for the user-specified dirs section"
msgstr "ব্যৱহাৰকৰ্তা দ্বাৰা চিহ্নিত dirs বিভাগৰ বাবে lock-down অৱস্থা"

#: ../main-menu/etc/slab.schemas.in.in.h:32
msgid "lock-down status for the user-specified docs section"
msgstr "ব্যৱহাৰকৰ্তা দ্বাৰা চিহ্নিত docs বিভাগৰ বাবে lock-down অৱস্থা"

#: ../main-menu/etc/slab.schemas.in.in.h:33
msgid "possible values = 0 [Applications], 1 [Documents], 2 [Places]"
msgstr "সম্ভাব্য মান = 0 [Applications], 1 [Documents], 2 [Places]"

#: ../main-menu/etc/slab.schemas.in.in.h:34
msgid ""
"set to true if the link to the application browser should be visible and "
"active."
msgstr ""
"অনুপ্ৰয়োগ চৰকৰ লিংক প্ৰদৰ্শন আৰু সক্ৰিয় কৰা হ'ব নে নহয় চিহ্নিত কৰাৰ বাবে মান "
"true নিৰ্ধাৰণ কৰক ।"

#: ../main-menu/etc/slab.schemas.in.in.h:35
msgid "set to true if the search area should be visible and active."
msgstr ""
"search area প্ৰদৰ্শন আৰু সক্ৰিয় কৰা হ'ব নে নহয় চিহ্নিত কৰাৰ বাবে মান true "
"নিৰ্ধাৰণ কৰক ।"

#: ../main-menu/etc/slab.schemas.in.in.h:36
msgid "set to true if the status area should be visible and active."
msgstr ""
"status area প্ৰদৰ্শন আৰু সক্ৰিয় কৰা হ'ব নে নহয় চিহ্নিত কৰাৰ বাবে মান true "
"নিৰ্ধাৰণ কৰক ।"

#: ../main-menu/etc/slab.schemas.in.in.h:37
msgid "set to true if the system area should be visible and active."
msgstr ""
"system area প্ৰদৰ্শন আৰু সক্ৰিয় কৰা হ'ব নে নহয় চিহ্নিত কৰাৰ বাবে মান true "
"নিৰ্ধাৰণ কৰক ।"

#: ../main-menu/etc/slab.schemas.in.in.h:38
msgid "set to true if the user is allowed to modify the list of system items."
msgstr ""
"ব্যৱহাৰকৰ্তা দ্বাৰা প্ৰণালীৰ বিভিন্ন সৰঞ্জামৰ তালিকা সম্পাদনযোগ্য হ'লে মান true "
"নিৰ্ধাৰণ কৰক ।"

#: ../main-menu/etc/slab.schemas.in.in.h:39
msgid ""
"set to true if the user is allowed to modify the list of user-specified or "
"\"Favorite\" applications."
msgstr ""
"ব্যৱহাৰকৰ্তা দ্বাৰা ব্যৱহাৰকৰ্তাদেৰ নিৰ্ধাৰিত অথবা \"পছন্দৰ\" অনুপ্ৰয়োগৰ তালিকা "
"সম্পাদনযোগ্য হ'লে মান true নিৰ্ধাৰণ কৰক ।"

#: ../main-menu/etc/slab.schemas.in.in.h:40
msgid ""
"set to true if the user is allowed to modify the list of user-specified or "
"\"Favorite\" directories or \"Places\"."
msgstr ""
"ব্যৱহাৰকৰ্তা দ্বাৰা ব্যৱহাৰকৰ্তাদেৰ নিৰ্ধাৰিত অথবা \"পছন্দৰ\" পঞ্জিকাৰ তালিকা "
"অথবা \"স্থান\" সম্পাদনযোগ্য হ'লে মান true নিৰ্ধাৰণ কৰক ।"

#: ../main-menu/etc/slab.schemas.in.in.h:41
msgid ""
"set to true if the user is allowed to modify the list of user-specified or "
"\"Favorite\" documents."
msgstr ""
"ব্যৱহাৰকৰ্তা দ্বাৰা ব্যৱহাৰকৰ্তাদেৰ নিৰ্ধাৰিত অথবা \"পছন্দৰ\" পঞ্জিকাৰ তালিকা "
"সম্পাদনযোগ্য হ'লে মান true নিৰ্ধাৰণ কৰক ।"

#: ../main-menu/src/hard-drive-status-tile.c:102
msgid "_System Monitor"
msgstr "প্ৰণালী নিৰীক্ষণ ব্যৱস্থা (_S)"

#: ../main-menu/src/hard-drive-status-tile.c:215
#, c-format
msgid "%.1fG"
msgstr "%.1fG"

#: ../main-menu/src/hard-drive-status-tile.c:217
#, c-format
msgid "%.1fM"
msgstr "%.1fM"

#: ../main-menu/src/hard-drive-status-tile.c:219
#, c-format
msgid "%.1fK"
msgstr "%.1fK"

#: ../main-menu/src/hard-drive-status-tile.c:221
#, c-format
msgid "%.1fb"
msgstr "%.1fb"

#: ../main-menu/src/hard-drive-status-tile.c:242
#, c-format
msgid "Home: %s Free / %s"
msgstr "ব্যক্তিগত ফোল্ডাৰ: %s মুক্ত / %s"

#: ../main-menu/src/main-menu-migration.c:135
#: ../main-menu/src/main-menu-migration.c:257
#: ../main-menu/etc/system-items.xbel.in.h:3
msgid "Logout"
msgstr "প্ৰস্থান কৰক"

#: ../main-menu/src/main-menu-migration.c:139
#: ../main-menu/src/main-menu-migration.c:259
#: ../main-menu/etc/system-items.xbel.in.h:4
msgid "Shutdown"
msgstr "বন্ধ কৰক"

#: ../main-menu/src/main-menu-migration.c:197
#: ../main-menu/etc/system-items.xbel.in.h:2
msgid "Lock Screen"
msgstr "পৰ্দা লক কৰক"

#: ../main-menu/src/main-menu-migration.c:199
msgid "gnome-lockscreen"
msgstr "gnome-lockscreen"

#: ../main-menu/src/main-menu-ui.c:2392
msgid "The GNOME Main Menu"
msgstr "GNOME Main Menu"

#: ../main-menu/src/network-status-tile.c:91
msgid "Network: None"
msgstr "নে'টৱৰ্ক: শূণ্য"

#: ../main-menu/src/network-status-tile.c:94
#: ../main-menu/src/network-status-tile.c:207
msgid "Click to configure network"
msgstr "নে'টৱৰ্ক বিন্যাস কৰাৰ বাবে ক্লিক কৰক"

#: ../main-menu/src/network-status-tile.c:206
msgid "Networ_k: None"
msgstr "নে'টৱৰ্ক: শূণ্য (_k)"

#: ../main-menu/src/network-status-tile.c:215
#: ../main-menu/src/network-status-tile.c:232
#: ../main-menu/src/network-status-tile.c:240
#, c-format
msgid "Connected to: %s"
msgstr "চিহ্নিত নে'টৱৰ্কৰ সৈতে সংযুক্ত: %s"

#: ../main-menu/src/network-status-tile.c:218
msgid "Networ_k: Wireless"
msgstr "নে'টৱৰ্ক: বেতাঁৰ (_k)"

#: ../main-menu/src/network-status-tile.c:223
#, c-format
msgid "Using ethernet (%s)"
msgstr "ইথাৰনেট ব্যৱহাৰ কৰা হৈছে (%s)"

#: ../main-menu/src/network-status-tile.c:227
msgid "Networ_k: Wired"
msgstr "নে'টৱৰ্ক: তাৰযুক্ত (_k)"

#: ../main-menu/src/network-status-tile.c:235
msgid "Networ_k: GSM"
msgstr "নে'টৱৰ্ক: GSM (_k)"

#: ../main-menu/src/network-status-tile.c:243
msgid "Networ_k: CDMA"
msgstr "নে'টৱৰ্ক: CDMA (_k)"

#: ../main-menu/src/network-status-tile.c:346
#, c-format
msgid "Wireless Ethernet (%s)"
msgstr "বেতাৰ ইথাৰনেট (%s)"

#: ../main-menu/src/network-status-tile.c:351
#, c-format
msgid "Wired Ethernet (%s)"
msgstr "তাৰযুক্ত ইথাৰনেট (%s)"

#: ../main-menu/src/network-status-tile.c:356
#: ../main-menu/src/network-status-tile.c:361
#, c-format
msgid "Mobile Ethernet (%s)"
msgstr "মোবাইল ইথাৰনেট (%s)"

#: ../main-menu/src/network-status-tile.c:365
#: ../main-menu/src/network-status-tile.c:375
#, c-format
msgid "Unknown"
msgstr "অজ্ঞাত"

#: ../main-menu/src/network-status-tile.c:373
#, c-format
msgid "%d Mb/s"
msgstr "%d Mb/s"

#: ../main-menu/src/slab-window.glade.h:1
msgid "Applications"
msgstr "অনুপ্ৰয়োগ"

#: ../main-menu/src/slab-window.glade.h:2
msgid "Computer"
msgstr "কম্পিউটাৰ"

#: ../main-menu/src/slab-window.glade.h:4
msgid "Favorite Applications"
msgstr "পছন্দৰ অনুপ্ৰয়োগ"

#: ../main-menu/src/slab-window.glade.h:5
msgid "Favorite Documents"
msgstr "পছন্দৰ নথিপত্ৰ"

#: ../main-menu/src/slab-window.glade.h:6
msgid "Favorite Places"
msgstr "পছন্দৰ স্থান"

#: ../main-menu/src/slab-window.glade.h:8
msgid "More Applications..."
msgstr "অতিৰিক্ত অনুপ্ৰয়োগ..."

#: ../main-menu/src/slab-window.glade.h:9
msgid "More Documents..."
msgstr "অতিৰিক্ত নথিপত্ৰ..."

#: ../main-menu/src/slab-window.glade.h:10
msgid "More Places..."
msgstr "অতিৰিক্ত স্থান..."

#: ../main-menu/src/slab-window.glade.h:11
msgid "Places"
msgstr "স্থান"

#: ../main-menu/src/slab-window.glade.h:12
msgid "Recent Applications"
msgstr "সম্প্ৰতি ব্যৱহৃত অনুপ্ৰয়োগ"

#: ../main-menu/src/slab-window.glade.h:13
msgid "Recent Documents"
msgstr "সৰ্বশেষ ব্যৱহৃত নথিপত্ৰৰ তালিকা"

#: ../main-menu/src/slab-window.glade.h:14
msgid "Search:"
msgstr "অনসুন্ধান:"

#: ../main-menu/src/slab-window.glade.h:15
msgid "Status"
msgstr "অৱস্থা"

#: ../main-menu/src/slab-window.glade.h:16
msgid "System"
msgstr "প্ৰণালী"

#: ../nautilus-main-menu/nautilus-main-menu.c:115
msgid "Add the current launcher to favorites"
msgstr "পছন্দৰ তালিকাত বৰ্তমান লঞ্চাৰ যোগ কৰক"

#: ../nautilus-main-menu/nautilus-main-menu.c:120
msgid "Add the current document to favorites"
msgstr "পছন্দৰ তালিকাত বৰ্তমান নথি যোগ কৰক"

#: ../nautilus-main-menu/nautilus-main-menu.c:125
#: ../nautilus-main-menu/nautilus-main-menu.c:130
msgid "Remove the current document from favorites"
msgstr "পছন্দৰ তালিকাৰ পৰা বৰ্তমান নথি আঁতৰাওক"

